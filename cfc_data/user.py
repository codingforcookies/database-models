import sqlalchemy as sq
import sqlalchemy.orm as orm

from . import db


class User(db.Model):
    __tablename__ = 'users'

    user_id = sq.Column(sq.Integer, primary_key=True)
    email = sq.Column(sq.String(320))
    nickname = sq.Column(sq.String(32))
    picture_url = sq.Column(sq.Text)
    role = sq.Column(sq.String(8))

    total_usage = sq.Column(sq.Integer)
    total_available = sq.Column(sq.Integer)

    def __init__(self, email, nickname, picture_url):
        self.email = email
        self.nickname = nickname
        self.picture_url = picture_url
        self.role = 'user' if User.query.first() is not None else 'admin'
        self.total_usage = 0
        self.total_available = 1024 * 1024 * 1024

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return self.user_id

    def as_dict(self):
        return {
                'user_id': self.user_id,
                'email': self.email,
                'nickname': self.nickname,
                'picture_url': self.picture_url,
                'role': self.role,
                'usage': { 'used': self.total_usage, 'available': self.total_available}
            }

    def __repr__(self):
        return '<User %r>' % self.user_id
