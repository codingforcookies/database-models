from datetime import datetime
import sys

import sqlalchemy as sq
import sqlalchemy.orm as orm
from sqlalchemy import extract

from . import db
from .api_key import APIKey
from .application import Application
from .user import User

class ServiceUsage(db.Model):
    __tablename__ = 'service_usage'

    usage_id = sq.Column(sq.Integer, primary_key=True, autoincrement=True)
    application_id = sq.Column(sq.String(32), sq.ForeignKey('applications.app_id'))
    service_id = sq.Column(sq.String(16))
    date = sq.Column(sq.Date)
    usage_bytes = sq.Column(sq.Integer)

    rel_usage = orm.relationship('Application', backref=orm.backref('usage', lazy='dynamic'))

    def __init__(self, service_id):
        self.service_id = service_id
        self.consumer_pair_id = consumer_pair_id
        self.date = datetime.utcnow()
        self.usage_bytes = 0

    def __repr__(self):
        return '<Service Usage %s on %r>' % (self.service_id, self.date)



def validate_key(service_id, consumer_secret=None, consumer_key=None):
    api_key = None

    if consumer_secret is not None:
        api_key = APIKey.query.filter_by(service_id=service_id, is_secret=True, key=consumer_secret).first()
        if api_key is None:
            return 'Unknown consumer secret.'
    elif consumer_key is not None:
        api_key = APIKey.query.filter_by(service_id=service_id, is_secret=False, key=consumer_key).first()
        if api_key is None:
            return 'Unknown consumer key.'
    else:
        return 'No key specified.'

    application = Application.query.filter_by(app_id=api_key.app_id).first()
    user = User.query.filter_by(user_id=application.user_id).first()

    return {
                'user': user,
                'app': application,
                'key': api_key
            }

def calc_bytes(data):
    return sys.getsizeof(data)

def use_service(service_id, user_id, key_id, requested_usage):
    user = User.query.filter_by(user_id=user_id).first()
    if user is None:
        return False, 'Invalid User ID.'
    if user.total_usage >= user.total_available:
        return False, 'You have reached your monthly cap.'
    if requested_usage <= 0:
        return False, 'You cannot send zero data.'
    if user.total_usage + requested_usage >= user.total_available:
        return False, 'Sending this data would result in exceeding your monthly cap.'
    now = datetime.utcnow()
    today_usage = ServiceUsage.query.filter(ServiceUsage.service_id==service_id, ServiceUsage.user_id==user_id, extract('year', ServiceUsage.date)==now.year, extract('month', ServiceUsage.date)==now.month, extract('day', ServiceUsage.date)==now.day).first()
    if today_usage is None:
        today_usage = ServiceUsage(service_id, user_id, consumer_pair_id)
    user.total_usage += requested_usage
    today_usage.usage_bytes += requested_usage
    db.session.add(today_usage)
    db.session.commit()
    return True, None
