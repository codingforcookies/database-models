import random
import hashlib
import base64
import binascii
from datetime import datetime, timedelta

import sqlalchemy as sq
import sqlalchemy.orm as orm

from . import db, secret_key


class APIKey(db.Model):
    __tablename__ = 'api_keys'

    key_id = sq.Column(sq.Integer, primary_key=True)
    app_id = sq.Column(sq.String(24), sq.ForeignKey('applications.app_id'))
    service_id = sq.Column(sq.String(16))
    name = sq.Column(sq.String(32))
    is_secret = sq.Column(sq.Boolean)
    key = sq.Column(sq.String(64), unique=True)

    rel_application = orm.relationship('Application', backref=orm.backref('api_keys', lazy='dynamic'))

    def __init__(self, app_id, service_id, name, is_secret):
        self.app_id = app_id
        self.service_id = service_id
        self.name = name

        self.is_secret = is_secret
        if is_secret:
            self.key = Generate.secret()[0]
        else:
            self.key = Generate.public()[0]

    def __repr__(self):
        return '<APIKey %s>' % self.name

class KeySession(db.Model):
    __tablename__ = 'key_sessions'

    session_id = sq.Column(sq.Integer, primary_key=True)
    key_id = sq.Column(sq.Integer, sq.ForeignKey('api_keys.key_id'))
    unique_id = sq.Column(sq.String(24), unique=True)
    born_time = sq.Column(sq.DateTime)
    last_used = sq.Column(sq.DateTime)
    expire_time = sq.Column(sq.DateTime)

    rel_api_key = orm.relationship('APIKey', backref=orm.backref('sessions', lazy='dynamic'))

    def __init__(self, key_id, unique_id):
        self.key_id = key_id
        self.unique_id = unique_id
        self.born_time = datetime.utcnow()

        self.refresh()

    def refresh(self):
        self.last_used = datetime.utcnow()
        self.expire_time = datetime.utcnow() + timedelta(days=365)

    def hash_me(self):
        weow = str(self.session_id)
        weow += str(self.born_time)
        weow += str(self.last_used)
        weow += str(self.expire_time)
        weow = hashlib.sha512(weow.encode('utf-8')).digest()
        return base64.b64encode(weow, 'fP'.encode('utf-8')).decode('utf-8')[:-2]

    def __repr__(self):
        return '<KeySession %s for %r>' % (self.session_id, self.key_id)

class KeySessionUsage(db.Model):
    __tablename__ = 'key_session_usage'

    usage_id = sq.Column(sq.Integer, primary_key=True)
    unique_id = sq.Column(sq.String(24), sq.ForeignKey('key_sessions.unique_id'))
    date = sq.Column(sq.DateTime)
    usage_bytes = sq.Column(sq.Integer)

    rel_session = orm.relationship('KeySession', backref=orm.backref('usage', lazy='dynamic'))

    def __init__(self, session_id):
        self.session_id = session_id
        self.date = datetime.utcnow()
        self.usage_bytes = 0

    def __repr__(self):
        return '<KeySession %s for %r>' % (self.service_id, self.date)

class Generate:
    def secret():
        return Generate.mk(64)

    def public():
        return Generate.mk(32)

    def mk(length):
        num_256bit = str(random.getrandbits(256)).encode('utf-8')
        hashed_num = hashlib.sha512(num_256bit).digest()
        char_pair = random.choice(['rA', 'aZ', 'gQ', 'hH', 'hG', 'aR', 'DD']).encode('utf-8')
        api_key = base64.b64encode(hashed_num, char_pair).decode('utf-8')[:length]
        return api_key, Generate.hash(api_key)

    def hash(api_key):
        key_hash = hashlib.pbkdf2_hmac('sha256', api_key.encode('utf-8'), secret_key.encode('utf-8'), 164877)
        key_hash = binascii.hexlify(key_hash)
        return key_hash.decode('utf-8')
