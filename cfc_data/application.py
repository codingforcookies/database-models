import random
import string

import sqlalchemy as sq
import sqlalchemy.orm as orm

from . import db

class Application(db.Model):
    __tablename__ = 'applications'

    app_id = sq.Column(sq.String(24), primary_key=True)
    user_id = sq.Column(sq.Integer, sq.ForeignKey('users.user_id'))
    name = sq.Column(sq.String(64))
    usage_bytes = sq.Column(sq.Integer)

    rel_user = orm.relationship('User', backref=orm.backref('applications', lazy='dynamic'))

    def __init__(self, user_id, name):
        self.app_id = ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(24))
        self.user_id = user_id
        self.name = name
        self.usage_bytes = 0

    def __repr__(self):
        return '<Application %s>' % self.app_id
