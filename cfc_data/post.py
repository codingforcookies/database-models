import sqlalchemy as sq
import sqlalchemy.orm as orm

from . import db


class Post(db.Model):
    __tablename__ = 'cfc_post'

    post_id = sq.Column(sq.Integer, primary_key=True)
    post_author_id = sq.Column(sq.Integer)
    post_title = sq.Column(sq.String(32))
    post_content = sq.Column(sq.Text)

    def __init__(self, post_author_id, post_title, post_content):
        self.post_author_id = post_author_id
        self.post_title = post_title
        self.post_content = post_content

    def get_author(self):
        return User.query.filter_by(user_id=self.post_author_id).first()

    def __repr__(self):
        return '<Post %s>' % self.post_title
