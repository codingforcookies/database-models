import os


SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL', 'sqlite:////tmp/test.db')
secret_key = 'mJ9bQy2vAGXEJMLcMbalZy4SHhHRzsBZdnlOAvoZtfXKzAoGB3x6NPZGdU3KJP7X'

db = None

class NoFlaskDB:
    def __init__(self):
        from sqlalchemy import create_engine
        from sqlalchemy.orm import scoped_session, sessionmaker
        from sqlalchemy.ext.declarative import declarative_base

        self.engine = create_engine(SQLALCHEMY_DATABASE_URI, convert_unicode=True)
        self.session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=self.engine))
        self.Model = declarative_base()
        self.Model.query = self.session.query_property()

    def create_all(self):
       self.Model.metadata.create_all(bind=self.engine)

def init(app=None):
    global db

    if app is not None:
        app.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        app.secret_key = secret_key

        from flask_sqlalchemy import SQLAlchemy
        db = SQLAlchemy(app)
    else:
        db = NoFlaskDB()

    import cfc_data.user
    import cfc_data.application
    import cfc_data.service
    import cfc_data.api_key

    import cfc_data.post

    db.create_all()
